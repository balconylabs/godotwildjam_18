## Story / Mechanics ideas
* Meta: Satire on gaming marketing. The premise is larger than the actual game 
  * Example: Game story about space miners. The actual mechanics is just dominos as there isn't shit to do in space waiting to get to an asteroid. 

## Learning goals
* Complete and publish a game
* Export game to desktop and mobile
* Gain experience in 2D art production
* Gain experience in 2D game development