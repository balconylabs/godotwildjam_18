extends Node

# TODO Implement object pool for better memory allocation of large swarms
#const Pool = preload("res://addons/godot-object-pool/pool.gd")

var NUM_ENEMIES = 100

const Enemy = preload("res://scenes/Enemy.tscn")
var enemies = {}


func _init():
	
	var instance
	
	for i in range(NUM_ENEMIES):
		instance = Enemy.instance()
		instance.global_position = Vector2(10,10-i*20)
		add_child(instance)
		

