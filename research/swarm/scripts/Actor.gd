extends KinematicBody2D
class_name Actor

const GRAVITY = 200.0
var WALK_SPEED = 200.0 setget set_walk_speed, get_walk_speed 
var velocity = Vector2()

func _physics_process(delta):
	applyGravity(delta)
	processInput()
	move()
	
func applyGravity(delta):
	velocity.y += delta * GRAVITY
	
func move():
	move_and_slide(velocity,Vector2(0,-1))	
	
func processInput():
	pass

func get_walk_speed():
	return WALK_SPEED
	
func set_walk_speed(speed):
	WALK_SPEED = speed
