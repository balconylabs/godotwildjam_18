extends Actor

var jumping = false
var JUMP_SPEED = 200.0

func _init():
	# for overriding variables
	#WALK_SPEED=
	#GRAVITY=
	pass
	
func processInput():
	
	if Input.is_action_just_pressed("jump"):
		if is_on_floor() and not jumping:
			velocity.y = -JUMP_SPEED
			jumping = true
	
	if Input.is_action_pressed("ui_left"):
		velocity.x = -WALK_SPEED
	elif Input.is_action_pressed("ui_right"):
		velocity.x = WALK_SPEED
	else:
		velocity.x = 0
		
	if jumping and is_on_floor():
		jumping = false
