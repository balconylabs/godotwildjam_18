extends Actor

onready var player = get_node("../Player")

var nutria_speed = speed.x * .75
var nutria_jump_speed = speed.y
var animated_sprite : AnimatedSprite
var velocity_lerp_weight = 0.01
var player_distance_threshold = 5

func _ready() -> void:
	
	#set_physics_process(false)
	_velocity.x = nutria_speed
	
	animated_sprite = get_node("AnimatedSprite")
	animated_sprite.play()

func _physics_process(delta: float) -> void:
	calculate_speed()
	calculate_jump()
	update_sprite_animation()
	move()

func calculate_speed():
	var new_velocity_x
	
	# Only seek out player if distance to player is greater than some threshold 
	# to avoid nutria from rapidly going left & right 
	if(abs(player.position.x - self.position.x) > player_distance_threshold):
		if player.position.x < self.position.x:
			new_velocity_x = -nutria_speed 
		else:
			new_velocity_x = nutria_speed
	else:
		new_velocity_x = 0
		
	_velocity.x = lerp(_velocity.x, new_velocity_x, velocity_lerp_weight)
		
func calculate_jump():
	if (is_on_wall() and 
			is_on_floor() and 
			abs(player.position.x - self.position.x) > player_distance_threshold):
		
		#do jump
		_velocity.y = -nutria_jump_speed
	
func update_sprite_animation():
	#set type of animation
	
	if is_in_water:
		#TODO swim stuff
		pass
	
	if _velocity.length() != 0: #if moving
		animated_sprite.play("Walk")
	else:
		animated_sprite.play("Idle")
	
	#flip sprite depending on direction of travel
	if not is_zero_approx(_velocity.x):
		if _velocity.x < 0:
			animated_sprite.flip_h = true
		if _velocity.x > 0:
			animated_sprite.flip_h = false
		
func move():
	_velocity.y = move_and_slide(_velocity, FLOOR_NORMAL).y
	#print(player.position.x, " / ", self.position.x)
