extends Actor

var player_speed = speed
var player_water_speed = player_speed * 0.65 

func _physics_process(delta: float) -> void:
	var is_jump_interrupted: = Input.is_action_just_released("jump") and _velocity.y < 0.0
	var direction: = get_direction()
	reduce_speed_if_in_water()
	_velocity = calculate_move_velocity(_velocity, direction, player_speed, is_jump_interrupted)
	var snap: Vector2 = Vector2.DOWN * 60.0 if direction.y == 0.0 else Vector2.ZERO
	_velocity = move_and_slide_with_snap(
		_velocity, snap, FLOOR_NORMAL, true
	)
	#print(player_speed)

func get_direction() -> Vector2:
	return Vector2(
		Input.get_action_strength("move_right") - Input.get_action_strength("move_left"),
		-Input.get_action_strength("jump") if is_on_floor() and Input.is_action_just_pressed("jump") else 0.0
	)

func calculate_move_velocity(
		linear_velocity: Vector2,
		direction: Vector2,
		player_speed: Vector2,
		is_jump_interrupted: bool
	) -> Vector2:
	var velocity: = linear_velocity
	velocity.x = player_speed.x * direction.x
	if direction.y != 0.0:
		velocity.y = player_speed.y * direction.y
	if is_jump_interrupted:
		velocity.y = 0.0
	return velocity

func reduce_speed_if_in_water():
	if is_in_water:
		player_speed = player_water_speed
	else:
		player_speed = speed

#func _on_Water_body_entered(body: Node) -> void:
#	#Need to ensure that the body that entered water was player
#	if(body.name == "Player"):
#		is_in_water = true
#
#		#print("Player In Water: ",is_in_water)
#		#print("body name: ",body.name)
#
#func _on_Water_body_exited(body: Node) -> void:
#	#Need to ensure that the body that entered water was player
#	if(body.name == "Player"):
#		is_in_water = false
#
#		#print("Player In Water: ",is_in_water)
#		#print("body name: ",body.name)
