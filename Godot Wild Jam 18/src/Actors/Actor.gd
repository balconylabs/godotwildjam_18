extends KinematicBody2D
class_name Actor


const FLOOR_NORMAL: = Vector2.UP

export var speed: = Vector2(400.0, 1000.0)
export var gravity: = 3500.0

var _velocity: = Vector2.ZERO
var is_in_water = false


func _physics_process(delta: float) -> void:
	_velocity.y += gravity * delta

func set_is_in_water(in_water: bool):
	is_in_water = in_water
