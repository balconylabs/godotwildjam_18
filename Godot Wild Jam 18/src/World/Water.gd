extends Area2D



func _on_Water_body_entered(body: Node):
	if body.has_method("set_is_in_water"):
		body.set_is_in_water(true)


func _on_Water_body_exited(body: Node):
	if body.has_method("set_is_in_water"):
		body.set_is_in_water(false)
